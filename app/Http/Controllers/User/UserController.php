<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = User::all();
        return view('admin.users.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = new User();

        return view('admin.users.edit', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        $data['password']= Hash::make($data['password']);
        $item = new User($data);
        $item->save();

        if($item){
            return  redirect()
            ->route('admin.user.edit', $item->id)
            ->with(['success'=>"Success saved"]);

        }
        else{
            return  back()
            ->withErrors(['msg'=>"Error during save process"])
            ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::findOrFail($id);

        return view('admin.users.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = User::find($id);
        if (empty($item)){
            return  back()
            ->withErrors(['msg'=>"data id:[{$id}] not found"])
            ->withInput();
        }

        $data= $request->all();
        
        if(empty($data['password'])){
            $data['password'] = $item->password;
        }

        $result= $item->update($data);
        if($result){
            return  redirect()
            ->route('admin.user.edit', $item->id)
            ->with(['success'=>"Success saved"]);

        }
        else{
            return  back()
            ->withErrors(['msg'=>"Error during save process"])
            ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
