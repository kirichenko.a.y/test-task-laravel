<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =[
            [
                'name' => 'test1',
                'email' =>'test@1.com',
                'password' => bcrypt(Str::random(10)),
            ],   
            [
                'name' => 'test2',
                'email' =>'2test@2.com',
                'password' => bcrypt(Str::random(10)),
            ]
        ];
  
          
        \DB::table('users')-> insert($data);
    }
}
