@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                <a class="btn btn-primary" href="{{route('admin.user.create')}}">Add new</a>
            </nav>

            <div class="card">
                <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                        @php /** @var  \App\Models\User $item */ @endphp
                            <tr>
                                <td>
                                    {{$item->id}}
                                </td>
                                <td>
                                    <a href="{{route('admin.user.edit', $item->id)}}">
                                        {{$item->name}}
                                    </a>
                                </td>
                                <td>
                                    {{$item->email}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>
                </div>            
            </div>
        </div>
    </div>
</div>
@endsection
