
@extends('layouts.app')

@section('content')
    @if ($item->exists)
        <form method="POST" action="{{route('admin.user.update', $item->id)}}">
        @method('PATCH')   
    @else
        <form method="POST" action="{{route('admin.user.store')}}">
    @endif
    
        @csrf
        <div class="container">
           
            @if ($errors->any())
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dissm="alert" aria-label="close">
                            <span aria-hidden="true">*</span>
                        </button>
                        {{$errors->first()}}
                    </div>
                </div>
            </div>
            @endif
            @if (session('success'))
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dissm="alert" aria-label="close">
                            <span aria-hidden="true">*</span>
                        </button>
                        {{session()->get('success')}}
                    </div>
                </div>
            </div>
            @endif
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-titel">Введите данные</div>
                                    <br>
                                    <div class="card-content">
                                        <div class="tab-pane active" id="maindata" role="tabpanel">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input name="name" value="{{$item->name}}"
                                                id="name"
                                                type="text"
                                                class="form-control"
                                                minlength="3"
                                                required>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input name="email" value="{{$item->email}}"
                                                id="email"
                                                type="text"
                                                class="form-control"
                                                required>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input name="password" value=""
                                                id="password"
                                                type="text"
                                                class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-11">
                    <div class="card">
                        <div class="card-body">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-primary" href="{{route('admin.user.index')}}">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
